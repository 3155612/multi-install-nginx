# multi-install-nginx

## 项目介绍
```
linux 下安装 nginx 的 bash 脚本。
跟进 nginx 和 openssl 官方版本进行脚本更新。
```

## 使用说明
```
脚本文件: multi-install-nginx.sh
脚本功能: Linux下编译安装nginx或tengine
脚本用法: bash multi-install-nginx.sh [nginx|tengine]
系统支持: Linux + Bash
版本要求: nginx 1.18+ | tengine 2.32+

将从官方下载的二进制 tar.gz 或 tar.xz 安装包文件放在脚本同级目录，运行脚本即可。

```

## 参考命令
```shell
# 安装
bash multi-install-nginx.sh install
```
```shell
# 卸载
bash multi-install-nginx.sh remove
```


## 注意事项
```
1、只支持 amd64 和 arm64 两个平台架构。
```

## 个人博客
```
https://seeyon.ren
```